import React from 'react';
import { create } from 'react-test-renderer';
import { OneReview } from '../OneReview';

describe('OneReview component', () => {
  test('Matches the snapshot', () => {
    const oneReview = create(
      <OneReview
        text="Porro officia cumque sint deleniti nemo facere rem vitae odit inventore cum odio, iste
              quia doloribus autem aperiam nulla ea neque reprehenderit. Libero doloribus, possimus
              officiis sapiente necessitatibus commodi consectetur?"
        author="Lourens S."
      />,
    );
    expect(oneReview.toJSON()).toMatchSnapshot();
  });
});
