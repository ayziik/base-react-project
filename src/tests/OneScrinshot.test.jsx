import React from 'react';
import { create } from 'react-test-renderer';
import { OneScrinshot } from '../OneScrinshot';

describe('OneScrinshot component', () => {
  test('Matches the snapshot', () => {
    const oneScrinshot = create(
      <OneScrinshot
        title="The description for the image"
        text="Pariatur iure ab sunt nesciunt, quibusdam odio iste cumque itaque, ipsa vel
              exercitationem ullam quos aut nostrum cupiditate fuga quaerat quam animi dolores.
              Sequi itaque, unde perferendis nemo debitis dolor."
      />,
    );
    expect(oneScrinshot.toJSON()).toMatchSnapshot();
  });
});
