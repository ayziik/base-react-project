import React from 'react';
import { create } from 'react-test-renderer';
import { Buy } from '../Buy';

describe('Buy component', () => {
  test('Matches the snapshot', () => {
    const buy = create(<Buy />);
    expect(buy.toJSON()).toMatchSnapshot();
  });
});
