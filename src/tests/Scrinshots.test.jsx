import React from 'react';
import { create } from 'react-test-renderer';
import { Scrinshots } from '../Scrinshots';

describe('Scrinshots component', () => {
  test('Matches the snapshot', () => {
    const scrinshots = create(<Scrinshots />);
    expect(scrinshots.toJSON()).toMatchSnapshot();
  });
});
