import React from 'react';
import { create } from 'react-test-renderer';
import { BuyCard } from '../BuyCard';

describe('BuyCard component', () => {
  test('Matches the snapshot', () => {
    const buyCard = create(
      <BuyCard
        title="Standart"
        price={100}
        list={[
          'Porro officia cumque sint deleniti;',
          'Тemo facere rem vitae odit;',
          'Cum odio, iste quia doloribus autem;',
          'Aperiam nulla ea neque.',
        ]}
      />,
    );
    expect(buyCard.toJSON()).toMatchSnapshot();
  });
});
