import React from 'react';
import { create } from 'react-test-renderer';
import { About } from '../About';

describe('About component', () => {
  test('Matches the snapshot', () => {
    const about = create(<About />);
    expect(about.toJSON()).toMatchSnapshot();
  });
});
