import React from 'react';
import { create } from 'react-test-renderer';
import { Reviews } from '../Reviews';

describe('Reviews component', () => {
  test('Matches the snapshot', () => {
    const reviews = create(<Reviews />);
    expect(reviews.toJSON()).toMatchSnapshot();
  });
});
