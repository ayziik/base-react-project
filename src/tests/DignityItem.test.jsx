import React from 'react';
import { create } from 'react-test-renderer';
import { DignityItem } from '../DignityItem';

describe('DignityItem component', () => {
  test('Matches the snapshot', () => {
    const dignityItem = create(
      <DignityItem
        text="Delectus dolorem vero quae beatae quasi dolor deserunt iste amet atque, impedit iure
          placeat, ullam. Reprehenderit aliquam, nemo cum velit ratione perferendis quas, maxime,
          quaerat porro totam, dolore minus inventore."
      />,
    );
    expect(dignityItem.toJSON()).toMatchSnapshot();
  });
});
