import React from 'react';
import { create } from 'react-test-renderer';
import { Contacts } from '../Contacts';

describe('Contacts component', () => {
  test('Matches the snapshot', () => {
    const contacts = create(<Contacts />);
    expect(contacts.toJSON()).toMatchSnapshot();
  });
});
