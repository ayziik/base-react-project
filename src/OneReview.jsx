import React from 'react';
import PropTypes from 'prop-types';

export function OneReview(props) {
  const { text, author } = props;

  return (
    <div className="reviews-content-wrapper">
      <div className="reviews__photo" />

      <div className="reviews-text-wrapper">
        <p className="reviews__text">{text}</p>

        <span className="reviews__author">{author}</span>
      </div>
    </div>
  );
}

OneReview.propTypes = {
  text: PropTypes.string.isRequired,
  author: PropTypes.string.isRequired,
};
