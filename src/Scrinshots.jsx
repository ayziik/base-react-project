import React from 'react';
import './scss/Scrinshots.scss';
import { OneScrinshot } from './OneScrinshot';

export function Scrinshots() {
  return (
    <section className="Scrinshots">
      <h2 className="scrinshots__title">Scrinshots</h2>

      <div className="scrinshots-wrapper">
        <OneScrinshot
          title="The description for the image"
          text="Pariatur iure ab sunt nesciunt, quibusdam odio iste cumque itaque, ipsa vel
              exercitationem ullam quos aut nostrum cupiditate fuga quaerat quam animi dolores.
              Sequi itaque, unde perferendis nemo debitis dolor."
        />

        <OneScrinshot
          title="The description for the image"
          text="Pariatur iure ab sunt nesciunt, quibusdam odio iste cumque itaque, ipsa vel
              exercitationem ullam quos aut nostrum cupiditate fuga quaerat quam animi dolores.
              Sequi itaque, unde perferendis nemo debitis dolor."
        />

        <OneScrinshot
          title="The description for the image"
          text="Pariatur iure ab sunt nesciunt, quibusdam odio iste cumque itaque, ipsa vel
              exercitationem ullam quos aut nostrum cupiditate fuga quaerat quam animi dolores.
              Sequi itaque, unde perferendis nemo debitis dolor."
        />

        <OneScrinshot
          title="The description for the image"
          text="Pariatur iure ab sunt nesciunt, quibusdam odio iste cumque itaque, ipsa vel
              exercitationem ullam quos aut nostrum cupiditate fuga quaerat quam animi dolores.
              Sequi itaque, unde perferendis nemo debitis dolor."
        />
      </div>
    </section>
  );
}
