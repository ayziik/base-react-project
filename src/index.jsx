import React from 'react';
import ReactDOM from 'react-dom';
import './scss/index.scss';
import { Header } from './Header';
import { About } from './About';
import { Dignity } from './Dignity';
import { Scrinshots } from './Scrinshots';
import { Reviews } from './Reviews';
import { Buy } from './Buy';
import { Contacts } from './Contacts';
// import * as serviceWorker from './serviceWorker';

ReactDOM.render(
  <React.StrictMode>
    <Header />
    <About />
    <Dignity />
    <Scrinshots />
    <Reviews />
    <Buy />
    <Contacts />
  </React.StrictMode>,
  document.getElementById('root'),
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
// serviceWorker.unregister();
