import React from 'react';
import './scss/Dignity.scss';
import { DignityItem } from './DignityItem';

export function Dignity() {
  return (
    <section className="Dignity">
      <h2 className="dignity__title">Dignity and pluses product</h2>

      <div className="dignity-wrapper">
        <DignityItem
          text="Delectus dolorem vero quae beatae quasi dolor deserunt iste amet atque, impedit iure
          placeat, ullam. Reprehenderit aliquam, nemo cum velit ratione perferendis quas, maxime,
          quaerat porro totam, dolore minus inventore."
        />

        <DignityItem
          text="Delectus dolorem vero quae beatae quasi dolor deserunt iste amet atque, impedit iure
          placeat, ullam. Reprehenderit aliquam, nemo cum velit ratione perferendis quas, maxime,
          quaerat porro totam, dolore minus inventore."
        />

        <DignityItem
          text="Delectus dolorem vero quae beatae quasi dolor deserunt iste amet atque, impedit iure
          placeat, ullam. Reprehenderit aliquam, nemo cum velit ratione perferendis quas, maxime,
          quaerat porro totam, dolore minus inventore."
        />

        <DignityItem
          text="Delectus dolorem vero quae beatae quasi dolor deserunt iste amet atque, impedit iure
          placeat, ullam. Reprehenderit aliquam, nemo cum velit ratione perferendis quas, maxime,
          quaerat porro totam, dolore minus inventore."
        />

        <DignityItem
          text="Delectus dolorem vero quae beatae quasi dolor deserunt iste amet atque, impedit iure
          placeat, ullam. Reprehenderit aliquam, nemo cum velit ratione perferendis quas, maxime,
          quaerat porro totam, dolore minus inventore."
        />

        <DignityItem
          text="Delectus dolorem vero quae beatae quasi dolor deserunt iste amet atque, impedit iure
          placeat, ullam. Reprehenderit aliquam, nemo cum velit ratione perferendis quas, maxime,
          quaerat porro totam, dolore minus inventore."
        />
      </div>
    </section>
  );
}
