import React from 'react';
import PropTypes from 'prop-types';

export function BuyCard(props) {
  const { list, title, price } = props;

  const listItems = list.map((item, index) => (
    <li key={index.toString()} className="buy__list-item">
      {item}
    </li>
  ));

  return (
    <div className="buy__card">
      <span className="buy__card-title">{title}</span>
      <span className="buy__price">${price}</span>
      <ol className="buy__list">{listItems}</ol>
      <button type="button" className="buy__btn">
        Buy
      </button>
    </div>
  );
}

BuyCard.propTypes = {
  list: PropTypes.instanceOf(Array).isRequired,
  title: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired,
};
