import React from 'react';
import './scss/Header.scss';

export function Header() {
  return (
    <div className="Header">
      <header>
        <div className="header-wrapper">
          <h1 className="header__title">Product name</h1>
          <ul className="header__bullets-list">
            <li className="header__bullet">Put on this page information about your product</li>
            <li className="header__bullet">A detailed description of your product</li>
            <li className="header__bullet">Tell us about the advantages and merits</li>
            <li className="header__bullet">Associate the page with the payment system</li>
          </ul>
        </div>

        <div className="header__photo"> </div>
      </header>
    </div>
  );
}
