import React from 'react';
import './scss/Reviews.scss';
import { OneReview } from './OneReview';

export function Reviews() {
  return (
    <section className="Reviews">
      <h2 className="reviews__title">Reviews</h2>

      <div className="reviews-wrapper">
        <OneReview
          text="Porro officia cumque sint deleniti nemo facere rem vitae odit inventore cum odio, iste
              quia doloribus autem aperiam nulla ea neque reprehenderit. Libero doloribus, possimus
              officiis sapiente necessitatibus commodi consectetur?"
          author="Lourens S."
        />

        <OneReview
          text="Porro officia cumque sint deleniti nemo facere rem vitae odit inventore cum odio, iste
              quia doloribus autem aperiam nulla ea neque reprehenderit. Libero doloribus, possimus
              officiis sapiente necessitatibus commodi consectetur?"
          author="Lourens S."
        />

        <OneReview
          text="Porro officia cumque sint deleniti nemo facere rem vitae odit inventore cum odio, iste
              quia doloribus autem aperiam nulla ea neque reprehenderit. Libero doloribus, possimus
              officiis sapiente necessitatibus commodi consectetur?"
          author="Lourens S."
        />

        <OneReview
          text="Porro officia cumque sint deleniti nemo facere rem vitae odit inventore cum odio, iste
              quia doloribus autem aperiam nulla ea neque reprehenderit. Libero doloribus, possimus
              officiis sapiente necessitatibus commodi consectetur?"
          author="Lourens S."
        />
      </div>
    </section>
  );
}
