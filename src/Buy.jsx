import React from 'react';
import './scss/Buy.scss';
import { BuyCard } from './BuyCard';

export function Buy() {
  return (
    <section className="Buy">
      <h2 className="buy__title">Buy it now</h2>

      <div className="buy-wrapper">
        <BuyCard
          title="Standart"
          price={100}
          list={[
            'Porro officia cumque sint deleniti;',
            'Тemo facere rem vitae odit;',
            'Cum odio, iste quia doloribus autem;',
            'Aperiam nulla ea neque.',
          ]}
        />

        <BuyCard
          title="Premium"
          price={150}
          list={[
            'Porro officia cumque sint deleniti;',
            'Тemo facere rem vitae odit;',
            'Cum odio, iste quia doloribus autem;',
            'Aperiam nulla ea neque.',
            'Porro officia cumque sint deleniti;',
            'Тemo facere rem vitae odit;',
            'Cum odio, iste quia doloribus autem;',
            'Aperiam nulla ea neque.',
          ]}
        />

        <BuyCard
          title="Lux"
          price={200}
          list={[
            'Porro officia cumque sint deleniti;',
            'Тemo facere rem vitae odit;',
            'Cum odio, iste quia doloribus autem;',
            'Aperiam nulla ea neque.',
            'Porro officia cumque sint deleniti;',
            'Тemo facere rem vitae odit;',
            'Cum odio, iste quia doloribus autem;',
            'Aperiam nulla ea neque.',
            'Porro officia cumque sint deleniti;',
            'Тemo facere rem vitae odit;',
            'Cum odio, iste quia doloribus autem;',
            'Aperiam nulla ea neque.',
          ]}
        />
      </div>
    </section>
  );
}
