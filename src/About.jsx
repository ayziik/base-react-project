import React from 'react';
import './scss/About.scss';

export function About() {
  return (
    <section className="About">
      <div className="about-wrapper">
        <h2 className="about__title">About your product</h2>
        <p className="about__text">
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nobis facilis fuga, illo at.
          Natus eos, eligendi illum rerum omnis porro ex, magni, explicabo veniam incidunt in quam
          sapiente ut ipsum.
        </p>
        <p className="about__text">
          Pariatur iure ab sunt nesciunt, quibusdam odio iste cumque itaque, ipsa vel exercitationem
          ullam quos aut nostrum cupiditate fuga quaerat quam animi dolores. Sequi itaque, unde
          perferendis nemo debitis dolor.
        </p>
      </div>

      <div className="about__photo" />
    </section>
  );
}
