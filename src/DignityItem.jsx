import React from 'react';
import PropTypes from 'prop-types';

export function DignityItem(props) {
  const { text } = props;

  return <p className="dignity__item">{text}</p>;
}

DignityItem.propTypes = {
  text: PropTypes.string.isRequired,
};
