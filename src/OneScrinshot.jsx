import React from 'react';
import PropTypes from 'prop-types';

export function OneScrinshot(props) {
  const { title, text } = props;

  return (
    <div className="scrinshots-content-wrapper">
      <div className="scrinshots__photo" />

      <div className="scrinshots-text-wrapper">
        <h3 className="scrinshots__item-title">{title}</h3>
        <p className="scrinshots__description">{text}</p>
      </div>
    </div>
  );
}

OneScrinshot.propTypes = {
  text: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
};
