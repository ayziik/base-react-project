import React from 'react';
import './scss/Contacts.scss';
import SVG from 'react-inlinesvg';

export function Contacts() {
  return (
    <section className="Contacts">
      <h2 className="contacts__title">Contacts</h2>

      <div className="contacts-wrapper">
        <form className="contacts__form">
          <input type="text" placeholder="Your name:" className="contacts__name-input" />
          <input type="text" placeholder="Your email:" className="contacts__email-input" />
          <textarea placeholder="Your message:" className="contacts__textarea" />
          <input type="submit" value="SEND" className="contacts__submit" />
        </form>

        <div className="social-wrapper">
          <div className="contacts__skype">here_your_login_skype</div>
          <div className="contacts__icq">279679659</div>
          <div className="contacts__email">psdhtmlcss@mail.ru</div>
          <div className="contacts__phone">80 00 4568 55 55</div>

          <div className="social-icons-wrapper">
            <a href="#" className="social-icon">
              <SVG src="../public/iconfinder_twitter.svg" width="44" height="44" />
            </a>
            <a href="#">
              <SVG src="../public/iconfinder_facebook.svg" width="44" height="44" />
            </a>
            <a href="#">
              <SVG src="../public/iconfinder_instagram.svg" width="44" height="44" />
            </a>
            <a href="#">
              <SVG src="../public/iconfinder_linkedin.svg" width="44" height="44" />
            </a>
            <a href="#">
              <SVG src="../public/iconfinder_youtube.svg" width="44" height="44" />
            </a>
          </div>
        </div>
      </div>
    </section>
  );
}
